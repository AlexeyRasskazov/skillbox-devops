
document.addEventListener("DOMContentLoaded", ready);

var canvas;
var canvasCtx;

// src pos
var ReC = -0.75;
var ImC = 0;
var ReL = 2.7;
var ImL;

// params
var nIterations = 200;
var nWidth;
var nHeight;
var CX, CY;
var nDrawRequestID;

/**
 *	Document ready.
 */
function ready() {

	canvas = document.querySelector('#canvas>canvas');
	canvasCtx = canvas.getContext("2d");
	nWidth = canvas.width;
	nHeight = canvas.height;
	CX = Math.floor(nWidth / 2);
	CY = Math.floor(nHeight / 2);
	ImL = ReL * nHeight/nWidth;
	canvasCtx.fillStyle = '#ffffff';
	canvasCtx.fillRect(0, 0, nWidth, nHeight);
	
	draw(0);
}

/**
 *	Private render function.
 */
function draw(pY) {
	const linesPerFrame = 5;
	for (i = 0; i < linesPerFrame; i++) {
		drawLine(pY + i * Math.sign(1));
	}

	if (pY > 0) {
		draw(-pY);
	} else {
		pY = -pY + linesPerFrame;
		if (CY + pY > nHeight) { 
			stop();
			return;
		}
		nDrawRequestID = window.requestAnimationFrame(()=>{ draw(pY) });
	}
}

function getRe(py) { return ReL*(py - CY)/nWidth + ReC; }
function getIm(px) { return ImL*(px - CX)/nHeight + ImC; }

function drawLine(pY)
{
	var py = CY + pY;
	var imageData = canvasCtx.getImageData(0, py, nWidth, 1);
	var data = imageData.data;
	var i = 0;
	var re = getRe(py);
	var clr = {r:0, g:0, b:0};

	for (var px = 0; px < nWidth; px++)
	{
		getMandelbrotColor(clr, re, getIm(px));
		data[i++] = clr.r;
		data[i++] = clr.g;
		data[i++] = clr.b;
		i++;
	}
	
	canvasCtx.putImageData(imageData, 0, py);
}

/**
 *	Stop visualizer
 */
function stop() {
	window.cancelAnimationFrame(nDrawRequestID);
}

/**
 *	Calculates pixel color for simple gradient.
 */
function getGradientColor(clr, re, im) {
	var p = 2*im/ImL;
	var m = 2*re/ReL;
	if (p < 0) p = -p;
	m = (m < 0) ?  1 + m : 1 - m; 
	clr.r = p < 0.5 ? Math.floor(m * 255*(1 - 2*p)) : 0;
	clr.g = Math.floor(m * 255*(2*(p < 0.5 ? p : 1 - p)));
	clr.b = p < 0.5 ? 0 : Math.floor(m * 255*(2*p - 1));
}

//var colorPos = [logScale(0.25), logScale(0.5), logScale(0.75)]
var colorPos = [0.25, 0.5, 0.75, 1];

/**
 *
 */
function getSpectrumColor(clr, val) {
	if (val <= colorPos[0]) {
		clr.r = 255;
		clr.g = Math.floor(255 * val / colorPos[0]);
		clr.b = 0;
	} else if (val <= colorPos[1]) {
		clr.r = Math.floor(255 * (colorPos[1] - val) / (colorPos[1] - colorPos[0]));
		clr.g = 255;
		clr.b = 0;
	} else if (val <= colorPos[2]) {
		clr.r = 0;
		clr.g = 255;
		clr.b = Math.floor(255 * (val - colorPos[1]) / (colorPos[2] - colorPos[1]));
	} else if (val <= colorPos[3]) {
		clr.r = 0;
		clr.g = Math.floor(255 * (1 - val) / (colorPos[3] - colorPos[2]));
		clr.b = 255;
	} else {
		clr.r = 0;
		clr.g = 0;
		clr.b = 255;
	}
}

/**
 *	Log scale of value [0..1]
 */
function logScale(val) {
	return Math.log(Math.max(1, val * 100)) / Math.log(100);
}

/**
 *	Calculates pixel color for Mandelbrot set.
 */
function getMandelbrotColor(clr, re0, im0) {
	var re = re0, im = im0, reN, imN;
	var k = 0;
	var ck = 5;
	var dk = ck, d;
	
	while (k < nIterations) {
		reN = re*re - im*im + re0;
		imN = 2*re*im + im0;
		d = reN*reN + imN*imN;
		if (d > 1e10) {			
			getSpectrumColor(clr, 1 - Math.min((k-10)/50, 1));
			return;
		}
		if (k == ck) dk = d;
		re = reN;
		im = imN;
		k++;
	}
	
	getSpectrumColor(clr, logScale(Math.min(dk/ck, 1)));
}
